<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <title>User Portal</title>
    <%@ include file="../jsp/partials/css.jsp" %>
    <%@ include file="../jsp/partials/js.jsp" %>

    <style>
        .my-nav-link{
            color: white;
        }
        .my-nav-link:hover{
            color: black;
        }

    </style>

</head>
<body style="overflow-x: hidden">
<nav class="navbar navbar-dark bg-primary">
    <a class="navbar-brand" href="#">Navgation</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
            aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation" id="navbarButton">
        <span class="navbar-toggler-icon"></span>
    </button>

</nav>
<div class="collapse navbar-collapse alert-dark p-2" id="navbarText"
     style="max-width: 500px ; min-width: 200px; z-index: 1; right: 20px; position: absolute">
    <ul class="navbar-nav mr-auto">
        <li class="nav-item active  btn btn-secondary  bg-primary m-1">
            <a class="nav-link my-nav-link" href="/logout" >Logout <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item  btn btn-secondary bg-primary m-1">
            <a class="nav-link my-nav-link" href="#" >Features</a>
        </li>
        <li class="nav-item btn btn-secondary  bg-primary m-1">
            <a class="nav-link my-nav-link" href="#" >Pricing</a>
        </li>
    </ul>
</div>

<div style="width: 100%;">

    <div class="container-fluid">
        <div style=" border: 1px solid black" class="row">
            <div class="col-lg-2" style="border: .5px solid black">

                <sec:authorize access="hasAuthority('admin')">
                    <a href="/UserList/" style="text-decoration: none ; border: none">
                        <div style=" border: .5px solid black" class="row">
                            <div class="col" style="border: .5px solid black">
                               User List
                            </div>
                        </div>
                    </a>
                </sec:authorize>
                <sec:authorize access="hasAuthority('user')">
                    <a href="/UserProfile/" style="text-decoration: none ; border: none">
                        <div style=" border: .5px solid black" class="row">
                            <div class="col" style="border: .5px solid black">
                                Profile Page
                            </div>
                        </div>
                    </a>
                    <a href="/ChangePassword/" style="text-decoration: none ; border: none">
                        <div style=" border: .5px solid black" class="row">
                            <div class="col" style="border: .5px solid black">
                                Change Password
                            </div>
                        </div>
                    </a>
                </sec:authorize>

            </div>
            <div class="col-lg-10" style="border: 1px solid black">
                <jsp:doBody/>
            </div>
        </div>
    </div>
</div>

</body>


</html>