<%--
  Created by IntelliJ IDEA.
  User: Nadim_PrsimERP
  Date: 14-Oct-19
  Time: 3:17 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <title>Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/resources/bootstrap-4.0.0/css/bootstrap.min.css"/>
</head>
<body>
<div class="container">
    <div class="card alert-danger" style="max-width: 400px ; margin: 150px auto">
        <div class="card-header">Error</div>
        <div class="card-body">
            Invalid URL or You are not allowed to access this url!!
        </div>
        <div class="card-footer">
            <sec:authorize access="hasRole('admin')">
                <a href="/UserList/">UserList</a>
            </sec:authorize>
            <sec:authorize access="hasRole('user')">
                <a href="/UserProfile/12/">Profile</a>
            </sec:authorize>
        </div>
    </div>
</div>
</body>
</html>
