<%--
  Created by IntelliJ IDEA.
  User: Nadim_PrsimERP
  Date: 14-Oct-19
  Time: 3:17 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/resources/bootstrap-4.0.0/css/bootstrap.min.css"/>
    <title>Title</title>
</head>
<body>
<div class="container">
    <div class="card" style="max-width: 600px ; margin: 20px auto">
        <div class="card-header">Registration</div>
        <div class="card-body">
            <form:form method="POST" modelAttribute="userEntity">
                <div class="row">
                    <div class="col-sm-6 form-group">
                        <form:label path="firstName">First Name</form:label>
                        <form:input path="firstName" cssClass="form-control"/>
                        <form:errors path="firstName" cssStyle="color: red"/>
                    </div>
                    <div class="col-sm-6 form-group">
                        <form:label path="lastName">Last Name</form:label>
                        <form:input path="lastName" cssClass="form-control"/>
                        <form:errors path="lastName" cssStyle="color: red"/>
                    </div>
                </div>
                <div class="form-group">
                    <form:label path="address">Address</form:label>
                    <form:textarea path="address" cssClass="form-control"/>
                    <form:errors path="address" cssStyle="color: red"/>
                </div>

                <div class="row">
                    <div class="col-sm-6 form-group">
                        <form:label path="phone">Phone</form:label>
                        <form:input path="phone" cssClass="form-control"/>
                        <form:errors path="phone" cssStyle="color: red"/>
                    </div>
                    <div class="col-sm-6 form-group">
                        <form:label path="email">Email</form:label>
                        <form:input path="email" cssClass="form-control"/>
                        <form:errors path="email" cssStyle="color: red"/>
                    </div>
                </div>

                <form:label path="birthDate">BirthDate</form:label>
                <form:input path="birthDate" type="date" cssClass="form-control"/>
                <form:errors path="birthDate" cssStyle="color: red"/>


                <form:label path="password">Password</form:label>
                <form:password path="password" cssClass="form-control"/>
                <form:errors path="password" cssStyle="color: red"/>


                <input type="submit" value="Register" class="btn btn-info btn-success" style="margin-top: 20px "/>

            </form:form>
        </div>
        <div class="card-footer"><a href="/">Already have an account?</a></div>

    </div>

</div>


</body>
</html>
