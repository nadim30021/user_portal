<%--
  Created by IntelliJ IDEA.
  User: Nadim_PrsimERP
  Date: 14-Oct-19
  Time: 3:17 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/resources/bootstrap-4.0.0/css/bootstrap.min.css"/>
</head>
<body>
<div class="container">
    <div class="card" style="max-width: 400px ; margin: 150px auto">
        <div class="card-header">Login</div>
        <div class="card-body">
            <form method="post">
                <label>Username</label> <input class="form-control" name="username">
                <label>Password</label> <input type="password" class="form-control" name="password">
                <div id="alert-div" class="alert-danger">
                    <c:if test="${param.error != null}">
                        <div class="alert-danger p-2 m-2">Invalid username or password.</div>
                    </c:if>
                </div>
                <div style="text-align: center">
                    <input type="submit" value="Login" class="btn btn-primary btn-success" style="margin-top: 20px ">
                </div>
            </form>
        </div>
        <div class="card-footer"><a href="/Registration/">Registration</a></div>
    </div>
</div>
</body>
</html>
