<%--
  Created by IntelliJ IDEA.
  User: Nadim_PrsimERP
  Date: 02-Oct-19
  Time: 3:09 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<script type="text/javascript" src="/jquery/jquery.js"></script>
<head>
    <title>Password Change</title>
</head>
<body>
<h3>Change Password</h3>
<form method="post" id="passChangeForm">
    <table>
        <tr>
            <td>Previous Password</td>
            <td><input type="text" name = "oldPass" id = "oldPass"/></td>

        </tr>
        <tr>
            <td>New Password</td>
            <td><input type="text" name = "newPass" id = "newPass"/></td>
        </tr>
        <tr>
            <td>Confirm Password</td>
            <td><input type="text" name = "confirmPass"  id = "confirmPass"/></td>
            <div name="passError" id="passError" style="color: red"></div>
            <div name="errorMessage" style="color: red">${errorMessage}</div>
        </tr>
        <input type="hidden"
               name="${_csrf.parameterName}"
               value="${_csrf.token}"/>
        <tr>
            <td><input type="submit" value="Submit"/></td>
        </tr>
    </table>
</form>


</body>
</html>

<script>
$("#passChangeForm").submit(function (e) {
    e.preventDefault();

    var newPass = $("#newPass").val();
    var confPass = $("#confirmPass").val();

    if(newPass!= confPass){
        $("#passError").html("New Password and Confirm Password should be same");
    }
    else{
        e.currentTarget.submit();
    }
})

</script>
