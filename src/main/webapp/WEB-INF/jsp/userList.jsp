<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:content>

    <link rel="stylesheet" type="text/css" href="/resources/DataTables/datatables.min.css"/>


    <div style="border-bottom: 1px solid black">
        <h2>User List</h2>
    </div>
    <br/>
    <h3 style="color: green">${successMessage}</h3>
    <br/>

    <table align="center" style="width: 100%;" id="userListTable" class="table table-striped">
        <thead>
        <th>Name</th>
        <th>Date Of Birth</th>
        <th>Email</th>
        <th>Phone</th>
        </thead>

        <tbody>
        <c:forEach items="${users}" var="user">
            <tr align="center" class="chart-alternative-row-fill">
                <td>${user.firstName} ${user.lastName}</td>
                <td>${user.birthDate}</td>
                <td>${user.email}</td>
                <td>${user.phone}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <script type="text/javascript" src="/resources/DataTables/datatables.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#userListTable").DataTable({
                responsive: {
                    details: true
                }
            });
        })
    </script>

</t:content>

