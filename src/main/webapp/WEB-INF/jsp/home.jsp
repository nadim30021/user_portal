<%--
  Created by IntelliJ IDEA.
  User: Nadim_PrsimERP
  Date: 02-Oct-19
  Time: 1:53 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Welcome to the jungle</title>
</head>
<body>
<h1>Welcome message : ${message}</h1>

<h3 style="color: greenyellow">${successMessage}</h3>
<table>
<c:forEach items="${users}" var="user">
    <tr>
        <td> Name : ${user.firstName}</td>
        <td> Email : ${user.email}</td>
    </tr>
</c:forEach>
</table>
</body>
</html>
