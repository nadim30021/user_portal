package com.nadim.user_portal.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "user", schema = "user_portal", catalog = "")
public class UserEntity {
    private int id;
    @NotBlank(message="First Name is required")
    private String firstName;
    private String lastName;
    private String address;
    private String phone;
    @NotBlank(message="Email is required")
    @Email(message = "Invalid email address")
    private String email;
    private Date birthDate;
    private String password;
    private Integer roleId;
    private UserRoleEntity userRoleByRoleId;

    public UserEntity(){

    }


    public UserEntity(UserEntity userEntity) {
            this.id = userEntity.getId();
            this.email = userEntity.getEmail();
            this.firstName = userEntity.getFirstName();
            this.lastName = userEntity.getLastName();
            this.address = userEntity.getAddress();
            this.phone = userEntity.getPhone();
            this.birthDate = userEntity.getBirthDate();
            this.password = userEntity.getPassword();
            this.roleId = userEntity.getRoleId();
            this.userRoleByRoleId = userEntity.getUserRoleByRoleId();
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "firstName")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "lastName")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "birthDate")
    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Basic
    @JsonIgnore
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "role_id" , insertable = false , updatable = false)
    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserEntity that = (UserEntity) o;
        return id == that.id &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(address, that.address) &&
                Objects.equals(phone, that.phone) &&
                Objects.equals(email, that.email) &&
                Objects.equals(birthDate, that.birthDate) &&
                Objects.equals(password, that.password) &&
                Objects.equals(roleId, that.roleId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, firstName, lastName, address, phone, email, birthDate, password, roleId);
    }

    @JsonManagedReference
    @ManyToOne
    @JoinColumn(name = "role_id", referencedColumnName = "id")
    public UserRoleEntity getUserRoleByRoleId() {
        return userRoleByRoleId;
    }

    public void setUserRoleByRoleId(UserRoleEntity userRoleByRoleId) {
        this.userRoleByRoleId = userRoleByRoleId;
    }
}
