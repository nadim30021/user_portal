package com.nadim.user_portal.Dao;

import com.nadim.user_portal.Entity.HomeEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HomeDao extends CrudRepository<HomeEntity, Integer> {

    List<HomeEntity> findAll();
}
