package com.nadim.user_portal.Dao;

import com.nadim.user_portal.Entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserDao extends JpaRepository<UserEntity, Integer> {

    List<UserEntity> findAll();

    UserEntity findById(int id);

    Optional<UserEntity> findByEmail(String s);
}
