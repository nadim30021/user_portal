package com.nadim.user_portal.Dao;

import com.nadim.user_portal.Entity.UserRoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRoleDao extends JpaRepository <UserRoleEntity , Integer> {

    UserRoleEntity findById(int id);

}
