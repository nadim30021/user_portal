package com.nadim.user_portal.Controller;

import com.nadim.user_portal.Dao.UserDao;
import com.nadim.user_portal.Dao.UserRoleDao;
import com.nadim.user_portal.Entity.UserEntity;
import com.nadim.user_portal.Service.LoggedInUserService;
import com.nadim.user_portal.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;

@Controller
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    UserDao userDao;

    @Autowired
    LoggedInUserService loggedInUserService;

    @RequestMapping(value = "/Registration/", method = RequestMethod.GET)
    public Object userAddForm(HttpServletRequest request, Model model) {
        UserEntity userEntity = new UserEntity();
        model.addAttribute("userEntity", userEntity);

        return "registration";
    }

    @RequestMapping(value = "/Registration/", method = RequestMethod.POST)
    public Object userAddFormSave(HttpServletRequest request, @Valid @ModelAttribute("userEntity") UserEntity user, BindingResult result, RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            return "registration";
        }

        redirectAttributes.addFlashAttribute("successMessage", "User has been added successfully");
        userService.save(user);

        return new RedirectView("/UserList/");
    }

    @RequestMapping(value = "/UserList/", method = RequestMethod.GET)
    public Object userList(HttpServletRequest request, Model model) {
        Object userList = userService.findAllData();
        model.addAttribute("users", userList);

        return "userList";
    }

    @RequestMapping(value = "/UserProfile/", method = RequestMethod.GET)
    public Object userProfile(HttpServletRequest request, Model model) throws Exception {
        UserEntity user = loggedInUserService.getLoggedInUser();
        model.addAttribute("user", user);

        return "userProfile";
    }

    @RequestMapping(value = "/ChangePassword/", method = RequestMethod.GET)
    public Object passwordChange(HttpServletRequest request) throws Exception {
        return "changePassword";
    }


    @RequestMapping(value = "/ChangePassword/", method = RequestMethod.POST)
    public Object passwordChangeSave(HttpServletRequest request, RedirectAttributes redirectAttributes) throws Exception {

        String oldPass = request.getParameter("oldPass");
        String newPass = request.getParameter("newPass");
        UserEntity user = loggedInUserService.getLoggedInUser();
        boolean result = userService.chekPasswordAndUpdate(oldPass, newPass, user.getId());
        if (result == true) {
            redirectAttributes.addFlashAttribute("successMessage", "User password updated successfully");
            return new RedirectView("/UserProfile/");
        } else {
            redirectAttributes.addFlashAttribute("errorMessage", "Failed to update user password!!");
            return new RedirectView("/ChangePassword/");
        }
    }


    @ResponseBody
    @RequestMapping(value = "/UserListWithResponseBody/", method = RequestMethod.GET)
    public Object userListWithResponseBody(HttpServletRequest request, Model model) {
        Object userList = userService.findAllData();
        model.addAttribute("users", userList);

        return userList;
    }

    @Autowired
    UserRoleDao userRoleDao;

    @ResponseBody
    @RequestMapping(value = "/UserRoleListWithResponseBody/", method = RequestMethod.GET)
    public Object userRoleListWithResponseBody(HttpServletRequest request, Model model) {
        Object userList = userService.findAllData();
        model.addAttribute("users", userList);

        return userRoleDao.findAll();
    }



}
