package com.nadim.user_portal.Controller;

import com.nadim.user_portal.Service.HomeService;
import com.nadim.user_portal.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
public class HomeController {

    @Autowired
    HomeService homeService;
    @Autowired
    UserService userService;


    @RequestMapping(value = {"/login", "/"})
    public Object login(HttpServletRequest request) {

        return "login";
    }

    @RequestMapping(value = "/error", method = RequestMethod.GET)
    public Object error() {
        return "error";
    }


}
