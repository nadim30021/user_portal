package com.nadim.user_portal.security;

import com.nadim.user_portal.Entity.CustomUserDetails;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;


@Component
public class MyAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    public MyAuthenticationSuccessHandler() {
        super();
        setUseReferer(true);
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {

        CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
        SavedRequest savedRequest = (SavedRequest) request.getSession().getAttribute("SPRING_SECURITY_SAVED_REQUEST");

        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();

        authorities.forEach(authority -> {
            if (savedRequest != null) {
                try {
                    response.sendRedirect(savedRequest.getRedirectUrl());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                if (authority.getAuthority().equals("user")) {
                    try {
                        redirectStrategy.sendRedirect(request, response, "/UserProfile/");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (authority.getAuthority().equals("admin")) {
                    try {
                        redirectStrategy.sendRedirect(request, response, "/UserList/");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }
}
