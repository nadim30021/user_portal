package com.nadim.user_portal.Service;

import com.nadim.user_portal.Dao.UserDao;
import com.nadim.user_portal.Entity.CustomUserDetails;
import com.nadim.user_portal.Entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    @Autowired
    UserDao userDao;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        Optional<UserEntity> userEntity = userDao.findByEmail(s);

        if(userEntity.isPresent()){
            return userEntity.map(CustomUserDetails::new).get();
        }
        else {
            throw new UsernameNotFoundException("User name not found");
        }
    }
}
