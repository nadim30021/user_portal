package com.nadim.user_portal.Service;

import com.nadim.user_portal.Entity.CustomUserDetails;
import com.nadim.user_portal.Entity.UserEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class LoggedInUserService {
    public UserEntity getLoggedInUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return  (CustomUserDetails) authentication.getPrincipal();
    }

}
