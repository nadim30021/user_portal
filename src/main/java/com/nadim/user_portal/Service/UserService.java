package com.nadim.user_portal.Service;

import com.nadim.user_portal.Dao.UserDao;
import com.nadim.user_portal.Dao.UserRoleDao;
import com.nadim.user_portal.Entity.UserEntity;
import com.nadim.user_portal.Entity.UserRoleEntity;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import sun.security.util.Password;

import java.util.List;
import java.util.Optional;


@Service
public class UserService {

    @Autowired
    UserDao userDao;

    @Autowired
    UserRoleDao userRoleDao;



    public Object findAllData(){
        List<UserEntity> userList =  userDao.findAll();

        return userList;
    }

    public Object save(UserEntity user) {
        UserEntity userEntity = new UserEntity();
        userEntity.setFirstName(user.getFirstName());
        userEntity.setLastName(user.getLastName());
        userEntity.setAddress(user.getAddress());
        userEntity.setBirthDate(user.getBirthDate());
        userEntity.setPhone(user.getPhone());
        userEntity.setEmail(user.getEmail());

//        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
//
//        String password = passwordEncoder.encode(user.getPassword());

        userEntity.setPassword(user.getPassword());
        userEntity.setRoleId(2);

        UserRoleEntity usrRoleEntity = userRoleDao.findById(2);

        userEntity.setUserRoleByRoleId(usrRoleEntity);

        return userDao.saveAndFlush(userEntity);
    }

    public boolean chekPasswordAndUpdate(String oldPass, String newPass, int id) {

        UserEntity userEntity = userDao.findById(id);

        if(userEntity.getPassword().equals(oldPass)){
            userEntity.setPassword(newPass);
            userDao.saveAndFlush(userEntity);
             return true;
        }
        else {
            return false;
        }
    }


}
